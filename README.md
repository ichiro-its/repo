# Repo

Personal repository that contains debian packages.

# Usage

## Add public key

```sh
~$ curl -s https://ichiro-its.gitlab.io/repo/ichiro.asc | sudo apt-key add -
```

## Add Repository to the sources list

```sh
~$ sudo sh -c 'echo "deb [arch=amd64] https://ichiro-its.gitlab.io/repo $(lsb_release -sc) main" > /etc/apt/sources.list.d/ichiro-latest.list'
~$ sudo apt update
```